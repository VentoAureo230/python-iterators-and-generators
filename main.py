import os


def readfiles(filenames: list):
    for filename in filenames:
        with open(filename, 'r') as file:
            for line in file:
                yield line


def print_long_lines(filename: str, length: int = 40) -> None:
    for line in readfiles([filename]):
        if len(line) > length:
            print(line)


def folder_to_listdir(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder)]


def recurive_file_listing(filename_or_foldername):
    """ List all files of a giver folder name. """
    if os.path.isdir(filename_or_foldername):
        for q in folder_to_listdir(filename_or_foldername):
            yield from recurive_file_listing(q)
    else:
        yield filename_or_foldername


""" Solution de print file avec os.walk
def print_files(foldername: str) -> None:
    for root, dirs, files in os.walk(foldername):
        for file in files:
            print(os.path.join(root, file))
"""


def print_files(foldername: str) -> None:
    """ print filenames recursively. """
    for filename in recurive_file_listing(foldername):
        print(filename)


def nb_python_files(foldername: str) -> int:
    i = 0
    for file in recurive_file_listing(foldername):
        print(file)
        if file.endswith('.py'):
            i = i + 1
            print(i)
    return i

def recursive_py_files(foldername) -> list:
    return [file for file in recurive_file_listing(foldername) if file.endswith('.py')]

def nb_lines_in_python_files(foldername) -> int:
    lines = [line.strip()]


def split_file(nb_lines: int, file: str):
    """ split a file"""
    pass


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_long_lines('data/file.txt')  # Affiche les deux premieres lignes du fichier
    print_files('data')
    assert nb_python_files('data') == 3
    assert nb_lines_in_python_files('data') == 25
    # split_file(12, 'data/subfolder/main.py')
    pass
