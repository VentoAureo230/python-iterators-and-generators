# Generateurs

Exemple de générateur

```python

def integers():
	"""Infinite sequence of integers."""
	i = 1
	while True:
		yield i
		i = i + 1
		
def squares():
	for i in integers():
		yield i * i
		
def take(n, seq):
	"""Returns first n values from the given sequence."""
	seq = iter(seq)
	result = []
	try:
		for i in range(n):
			result.append(next(seq))
	except StopIteration:
		pass
	return result
	
print(take(5, squares())) # prints [1, 4, 9, 16, 25]
```

Les générateurs anonymes sont des générateurs assez semblables à des listes.

```python
>>> a = (x*x for x in range(10))
>>> a

<generator object <genexpr> at 0x401f08>
>>> sum(a)
285
```

Il est possible d'utiliser ces générateurs anonymes comme paramètres de fonction. Ils fonctionnent comme des itérateurs.

```python
>>> sum((x*x for x in range(10)))
285
```

Lorsque la fonction attend un seul paramètre, il est possible d'ommettre les parenthèses autour du générateur anonyme.
```python
>>> sum(x*x for x in range(10))
285
```

Autre exemple:
Vous souhaitez trouver les 3 (ou n) premiers triplets pythagoriens. C'est à dire des triplets (x,y,z) respectant la condition suivante : `x*x + y*y == z*z`.

Ce problème est facile à résoudre tant qu'on connait la limite de z... Mais ici le sujet est posé d'une manière inversée.. On souhaite les 10 premiers... Avec le générateurs on résous le problème plutôt facilement.

```python
pyt = ((x, y, z) for z in integers() for y in range(1, z) for x in range(1, y) if (x*x + y*y == z*z) )
take(3, pyt)

# [(3, 4, 5), (6, 8, 10), (5, 12, 13)]
```

## Exemple: Lecture multiple de fichiers

Nous souhaitons implémenter un script prenant un ensemble de nom de fichier en paramètres. Le script doit afficher le contenu du fichier (~ cat file)

Usuellement, nous implémentons ceci :

```python
def cat(filenames):
	for f in filenames:
		for line in open(f):
			print(line)
```

Nous souhaitons implémenter une autre fonction qui recherche une string dans les fichiers (~ grep )

```python
def grep(pattern, filenames):
	for f in filenames:
		for line in open(f):
			if pattern in line:
				print(line)
```

Ces deux scripts ont une grosse partie de code en commun mais comment refactorer... Le générateur est adapté.

```python
def readfiles(filenames):
	for f in filenames:
		for line in open(f):
			yield line

def grep(pattern, lines):
	return (line for line in lines if pattern in line)

def printlines(lines):
	for line in lines:
		print(line),

def main(pattern, filenames):
	lines = readfiles(filenames)
	lines = grep(pattern, lines)
	printlines(lines)
```


## Exercices
Implémentez des générateurs et/ou itérateurs pour les exercices suivants.

### Lignes de plus de 40 caractères 
Ecrire une méthode `print_long_lines` prenant un ou plusieurs fichiers en paramètres et qui doit parcourir ces fichiers et afficher les lignes 
de plus de 40 lettres


##  Lister des fichiers: 
Implémentez la fonction `print_files`, qui lit un dossier d'une manière récursive et liste l'ensemble des fichiers avec leurs arborescence de dossier.
```
print_files('data')
> data/subfolder/again_a_subfolder/__init__.py
> data/subfolder/again_a_subfolder/module.py
> data/subfolder/main.py
> data/file.txt
```

##  Lister des fichiers avec filtre: 
Lister le nombre de fichiers python (ayant l'extension .py) au sein d'un dossier (récursivement)
```
nb_python_files(folder)
> 3
```
##  Afficher le nombre de lignes présents dans des fichiers python
A partir d'un nom de dossier, afficher le nombre total de lignes présents dans des fichiers python.
```
nb_lines_in_python_files(folder)
> 25
```
Attention, votre code ne doit pas prendre en compte les lignes vides ou commentées.

##  Split file: 
Implémentez une fonction `split_file`, prenant deux paramètres. Un entier n, et un nom de fichier filename.
Votre script doit scinder le fichier spécifié en sous fichers de n lignes.
```
split_file(12, 'data/subfolder/main.py')
> created file_1.py
> created file_2.py
> created file_3.py
```


# Les decorators

Au sein de Python, les functions sont des objects. Ils peuvent être passés en arguments d'autres fonctions.
ou peuvent être également retournés par d'autres fonctions. 

Example: Tracer les appels d'une méthode
Vous avez la suite de fibonacci d'implémentée. 

```python
def fib(n):
    if n is 0 or n is 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)
```

Et vous souhaitez tracer tous les appels de cette fonction. Il est possible de rajouter un décorateur sur cette fonctino. 

```python
def trace(f):
    def g(x):
        print(f.__name__, x)
        value = f(x)
        print('return', repr(value))
        return value
    return g
fib = trace(fib)
print(fib(3))
```

Ici, on a remplacé la méthode `fib` par une nouvelle fonction.
Et ainsi, nous avons rajouté un peu lde logs dans la suite d'ppels
Ajoutons un peu d'indentations à l'implémentation.
```python

def trace_i(f):
    f.indent = 0
    def g(x):
        print ('| ' * f.indent + '|--', f.__name__, x)
        f.indent += 1
        value = f(x)
        print( '| ' * f.indent + '|--', 'return', repr(value))
        f.indent -= 1
        return value
    return g

@trace_i
def fib(n):
    ...

print(fib(4))
```

Exemple: Memoize
Au sein de l'exemple précédent, on peut voir que le nombre d'appels augmente exponentiellement.
De nombreux calculs sont répétés.

Il est possible de stocker les valeurs préalablement calculées afin d'optimiser tout celà.
Cette méthode est usuellement nommée `memoize`.

```python

def memoize(f):
    cache = {}
    def g(x):
        if x not in cache:
            cache[x] = f(x)
        return cache[x]
    return g
fib = trace_i(fib)
fib = memoize(fib)

print(fib(4))
```

## Exercices : 
### Fonction profile
Implémenter une fonction `profile`, permettant de logger le temps d'éxecution d'une méthode sans aucun autre altération de l'appel.
```python

fib = profile(fib)
fib(20)
>>> time taken: 0.1 sec (10946)
```

### Fonction vectorize
Ecrire un décorateur `vectorize` prenant une liste d'arguments en paramètres et qui appelle la fonction décorée sur chaque élément de la liste. 

```python
@vectorize
def square(x): return x * x

square([1, 2, 3])
>>> [1,4,9]
g = vectorize(len)
g(["hello", "world"])
>>> [5,5]
```
